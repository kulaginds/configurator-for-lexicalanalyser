package edu.sevgu.kulaginds;

import java.io.*;

public class Configurator {

    protected int states_count = 0;
    protected int chars_count = 0;
    protected char[] chars;
    protected int[][] config;

    Configurator(String config_file, String table_file, String out_config_file) throws ConfiguratorException, IOException {
        if (config_file == null
                || config_file.length() == 0
                || table_file == null
                || table_file.length() == 0
                || out_config_file == null
                || out_config_file.length() == 0)
            throw new ConfiguratorException(ConfiguratorException.INPUT_MISSING);
        loadConfig(config_file);
        loadAndParse(table_file);
        saveConfig(out_config_file);
    }

    protected void loadConfig(String config) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(config));
        String line;
        while (null != (line = br.readLine()))
            if (states_count == 0)
                states_count = Integer.parseInt(line);
            else if (chars_count == 0) {
                chars_count = Integer.parseInt(line);
                break;
            }
        this.config = new int[chars_count][states_count];
        this.chars = new char[chars_count];
        for (int i=0; i<chars_count; i++)
            this.chars[i] = (char)br.read();
        br.close();
    }

    protected void loadAndParse(String table) throws ConfiguratorException, IOException {
        if (states_count == 0
                || chars_count == 0
                || chars == null
                || chars_count != chars.length)
            throw new ConfiguratorException(ConfiguratorException.CONFIG_MISSING);
        BufferedReader br = new BufferedReader(new FileReader(table));
        String line;
        String[] cells;
        int line_count = 1;
        while (null != (line = br.readLine())) {
            cells = line.split("\\|");
            if (cells.length != states_count)
                throw new ConfiguratorException(String.format(ConfiguratorException.LINE_MISSING_STATES, line_count));
            for (int i=0; i<cells.length; i++)
                config[(line_count - 1)][i] = parseCell(cells[i], (i + 1), line_count);
            line_count++;
        }
        br.close();
    }

    protected int parseCell(String input, int cell_num, int line_num) throws ConfiguratorException {
        int output = 0;
        String[] parts;
        if (input == null || input.length() == 0)
            throw new ConfiguratorException(String.format(ConfiguratorException.CELL_MISSING, cell_num, line_num));
        if (input.equals("E"))
            output = -1;
        else if (-1 == input.indexOf(','))
            output = Integer.parseInt(input.substring(1));
        else {
            parts = input.split(",");
            if (parts.length > 3)
                throw new ConfiguratorException(String.format(ConfiguratorException.CELL_MISSING, cell_num, line_num));
            output = Integer.parseInt(parts[0].substring(1)); // код состояния
            output += (Integer.parseInt(parts[1].substring(1)) << 1 * 8); // код лексемы
            if (parts.length == 3)
                output += (0xCC << 2 * 8); // сдвиг
        }
        return output;
    }

    protected void saveConfig(String out_config) throws IOException {
        int i, j = 0;
        DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(out_config));
        outputStream.writeByte(states_count);
        outputStream.writeByte(chars_count);
        for (i=0; i<chars_count; i++)
            outputStream.writeByte(chars[i]);
        for (i=0; i<chars_count; i++)
            for (j=0; j<states_count; j++) {
                outputStream.writeByte((byte)(0xFF & (config[i][j] >> 2 * 8))); // low byte
                outputStream.writeByte((byte)(0xFF & (config[i][j] >> 1 * 8))); // middle byte
                outputStream.writeByte((byte)(0xFF & config[i][j])); // high byte
            }
        outputStream.close();
    }

}
