package edu.sevgu.kulaginds;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        if (args == null || args.length < 3) {
            System.out.println("help:");
            System.out.println("<prog> config.txt table.txt out_config.dat");
            System.exit(-1);
        }

        String config = args[0],
                table = args[1],
                out_config = args[2];

        try {
            new Configurator(config, table, out_config);
        } catch (ConfiguratorException e) {
            System.err.println("configurator error: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("io error: " + e.getMessage());
        }
    }
}
