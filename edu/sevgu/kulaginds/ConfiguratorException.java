package edu.sevgu.kulaginds;

public class ConfiguratorException extends Exception {

    static final String INPUT_MISSING = "input arguments is missing";
    static final String CONFIG_MISSING = "config file is missing";
    static final String LINE_MISSING_STATES = "in line %d missing count of states";
    static final String CELL_MISSING = "cell %d in line %d is missing";

    ConfiguratorException(String msg) { super(msg); }

}
